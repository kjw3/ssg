# Project SSG

## Purpose

Project SSG is an endeavor to take the lessons I've learned on personal finance, morals and from [Dave Ramsey](http://daveramsey.com) over the years in combination to pass them on to future generations.

Our reliance on debt as a form of free living is completely backwards. We've been sold a bill of goods on building credit, student loans being good and car payments. That bill of goods is not what it promises.

This project will start with software to allow parents to work with their children to learn proper money management principals. The software will be heavily based on the principals that Dave Ramsey teaches. If you want to learn more from Dave, start with Total Money Makeover. As a parent, read Smart Money, Smart Kids.

## Why SSG?

Spend. Save. Give

## Installation

Soon: Download the Spend.Save.Give app in either Apple or Google app stores.

## Contribute

Use the App. Add features. Write an SSG blog post. Write documentation. File Bugs. Donate. Spread the word. It's the Open Source Way. Do what you can.

